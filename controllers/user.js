var mongoose = require('mongoose');
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var fs = require('fs');
var session = require('../lib/session');
var User = mongoose.model('user');
var Youtube = require("youtube-api");
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var connection = null;

var auth = new googleAuth();
var oauth2Client = new auth.OAuth2('972391797918-1n456jgnbv7g0o7shmcqhn1tp5sl84fv.apps.googleusercontent.com', 'W-BbeUl9-FqAhYM6vp_W8GtA', 'http://demo.omnisttechhub.com/api/google/callback');

var response = {
	error: false,
	status: 200,
	data: null,
	userMessage: '',
	errors: null
};

var NullResponseValue = function() {
	response = {
		error: false,
		status: 200,
		data: null,
		userMessage: '',
		errors: null
	};
	return true;
};
var SendResponse = function(res, status) {
	res.status(status || 200).send(response);
	NullResponseValue();
	return
};

var methods = {};

module.exports = function(router, passport) {

	passport.serializeUser(function(user, done) {
		done(null, user);
	});

	passport.deserializeUser(function(user, done) {
		done(null, user);
	});

	passport.use(new GoogleStrategy({
			clientID: '972391797918-1n456jgnbv7g0o7shmcqhn1tp5sl84fv.apps.googleusercontent.com' || '972391797918-dhqhda8oa4n6gp527lotra3vhf8jmtcl.apps.googleusercontent.com',
			clientSecret: 'W-BbeUl9-FqAhYM6vp_W8GtA' || 'Qot0aZC25xX1XVZvgA8my0cP',
			callbackURL: "http://demo.omnisttechhub.com/api/google/callback",
			passReqToCallback: true
		},
		function(request, accessToken, refreshToken, profile, done) {
			User.findOne({
				email: profile.email
			}, function(error, user) {
				if (error) {
					console.log("error ", error);
					done(null, profile);
				} else if (!user) {
					var user = new User({
						email: profile.email,
						accessToken: accessToken,
						refreshToken: refreshToken,
						name: profile.displayName,
						socialId: profile.id
					});
					user.save(function(err) {
						done(null, user);
					});
				} else {
					user.email = profile.email;
					user.name = profile.displayName;
					user.socialId = profile.id;
					user.accessToken = accessToken;
					user.refreshToken = refreshToken;
					user.save(function() {
						done(null, user);
					})
				}
			});
		}
	));


	router
		.route('/google')
		.get(passport.authenticate('google', {
			scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/plus.profile.emails.read', 'https://www.googleapis.com/auth/youtube']
		}));

	router
		.route('/google/callback')
		.get(passport.authenticate('google', {
			failureRedirect: '/error'
		}), function(req, res) {
			console.log(req.user);
			console.log('http://godatu.com?accessToken=' + req.user.accessToken);
			res.redirect('http://demo.omnisttechhub.com?accessToken=' + req.user.accessToken);
		});

	router
		.route('/channels')
		.get(session.checkToken, methods.getChannelList)

	router
		.route('/channels/:channelId/videos')
		.get(session.checkToken, methods.getVideos)

}

/*================================
***   get list of channels  ***
==================================*/

methods.getChannelList = function(req, res) {
	oauth2Client.credentials = {
		access_token: req.user.accessToken,
		refresh_token: req.user.refreshToken,
		token_type: 'Bearer'
	};
	var youtube = google.youtube({
		version: 'v3',
		auth: oauth2Client
	});

	youtube.channels.list({
		auth: oauth2Client,
		part: 'snippet',
		mine: true
	}, function(err, data) {
		console.log(data);
		//send response to client
		response.error = err ? true : false;
		response.status = err ? 500 : 200;
		response.errors = err;
		response.userMessage = 'list of channels';
		response.data = data;
		return SendResponse(res);
	})


}

/*-----  End of getChannelList  ------*/

/*==============================
***   get list of videos  ***
================================*/

methods.getVideos = function(req, res) {
	//Check for POST request errors.
	// 'https: //www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId=UC_x4m-1a5wPunvyLR77c7dA&maxResults=25&key=AIzaSyDmu_50yiHDH20o5ZLc58PgsO0hJEA2R7I'
	oauth2Client.credentials = {
		access_token: req.user.accessToken,
		refresh_token: req.user.refreshToken,
		token_type: 'Bearer'
	};
	var youtube = google.youtube({
		version: 'v3',
		auth: oauth2Client
	});

	youtube.search.list({
		order: 'date',
		auth: oauth2Client,
		part: 'snippet',
		channelId: req.params.channelId,
		mine: true
	}, function(err, data) {
		console.log(data);
		//send response to client
		response.error = err ? true : false;
		response.status = err ? 500 : 200;
		response.errors = err;
		response.userMessage = 'list of videos';
		response.data = data;
		return SendResponse(res);
	})
}

/*-----  End of getVideos  ------*/