var mongoose = require('mongoose');
var User = mongoose.model('user');
var ObjectId = mongoose.Schema.ObjectId;

var session = {};

var response = {
	error: false,
	status: "",
	data: null,
	userMessage: ''
};
var NullResponseValue = function() {
	response = {
		error: false,
		status: "",
		data: null,
		userMessage: '',
		errors: null
	};
	return true;
};
var SendResponse = function(res, status) {
	response.status = status || 200;
	res.status(status || 200).send(response);
	NullResponseValue()
	return
};


/*********************
	Checking for token of admin user
*********************/


session.checkToken = function(req, res, next) {
	var bearerToken;
	var bearerHeader = req.headers["authorization"];
	if (typeof(bearerHeader) !== 'undefined') {

		var bearer = bearerHeader.split(" ");
		bearerToken = bearer[1];
		req.token = bearerToken;

	}
	var token = bearerToken || req.body.token || req.query.token;
	User.findOne({
			accessToken: token
		})
		.lean()
		.exec(function(err, user) {
			console.log(err);
			if (err || !user) {
				response.userMessage = "Your session has been expired. Please relogin.";
				return SendResponse(res, 401);
			} else {
				req.user = user;

				next();
			}
		});
};

/*********************
	checkToken Ends
*********************/

module.exports = session;