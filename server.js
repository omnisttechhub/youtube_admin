require('dotenv').config();
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var cors = require('cors');
var passport = require('passport');
var fs = require('fs');
var path = require('path');

var PORT = process.env.PORT || 8000;
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
	extended: false
}));

// parse application/json
app.use(bodyParser.json());
app.use(expressValidator());

app.use(passport.initialize());

var router = express.Router();

app.use(cors());

app.use('/api', router);

// Connect to mongodb
var connect = function() {
	var options = {
		server: {
			socketOptions: {
				keepAlive: 1
			}
		}
	};
	mongoose.connect('mongodb://localhost:27017/youtube', options);
};
connect();
mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);

// Bootstrap models
fs.readdirSync('models').forEach(function(file) {
	if (~file.indexOf('.js')) require('./models/' + file);
});


// Bootstrap controllers
fs.readdirSync('controllers').forEach(function(file) {
	if (~file.indexOf('.js')) {
		require('./controllers/' + file)(router, passport);
	}
});

app.listen(PORT, function() {
	console.log("server is listening on ", PORT);
})