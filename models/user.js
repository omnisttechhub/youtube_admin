var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;


var user = new Schema({
	name: {
		type: String,
		default: ''
	},
	email: {
		type: String,
		default: ''
	},
	socialId: {
		type: String,
		default: ''
	},
	accessToken: {
		type: String,
		default: ''
	},
	refreshToken: {
		type: String,
		default: ''
	},
	createdAt: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('user', user);